ABR_FoodPredator
==============
This is the Android application for running the predator robot in our 
publication titled "The Advantage of Prediction and Mental Imagery for 
Goal-Directed Behavior in Agents and Robots". Main_activity.java is the primary
start point for running the app, while AgentForage.java contains the majority
of code to run the planning algorithms of the model.