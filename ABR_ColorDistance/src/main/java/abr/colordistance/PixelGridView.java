package abr.colordistance;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import org.opencv.core.Point;


/**
 * Created by tiffany on 7/10/16.
 */
public class PixelGridView extends View {
    private int numColumns, numRows;
    private int cellWidth, cellHeight;
    private Paint paint = new Paint();
    private boolean[][] cellChecked;
    private int[][] gridColors = new int[0][0];

    public PixelGridView(Context context) {
        this(context, null);
    }

    public PixelGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
    }

    public int getNumColumns() {
        return numColumns;
    }

    public void setNumColumns(int numColumns) {
        this.numColumns = numColumns;
        calculateDimensions();
    }

    public int getNumRows() {
        return numRows;
    }

    public void setNumRows(int numRows) {
        this.numRows = numRows;
        calculateDimensions();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        calculateDimensions();
    }

    private void calculateDimensions() {
        if (numColumns < 1 || numRows < 1) {
            return;
        }

        cellWidth = getWidth() / numColumns;
        cellHeight = getHeight() / numRows;

        cellChecked = new boolean[numColumns][numRows];

        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.WHITE);

        if (numColumns == 0 || numRows == 0) {
            return;
        }

        int width = getWidth();
        int height = getHeight();

        for (int i = 0; i < numColumns; i++) {
            for (int j = 0; j < numRows; j++) {
                if(gridColors[j][i] == 0) {
                    float[] col = {(float) gridColors[j][i], (float) .1, (float) 1};
                    paint.setColor(Color.HSVToColor(col));
                }
                else {
                    float[] col = {(float) gridColors[j][i], (float) 1, (float) 1};
                    paint.setColor(Color.HSVToColor(col));
                }
                canvas.drawRect(i * cellWidth, j * cellHeight,
                        (i + 1) * cellWidth, (j + 1) * cellHeight,
                        paint);
            }
        }

        for (int i = 1; i < numColumns; i++) {
            canvas.drawLine(i * cellWidth, 0, i * cellWidth, height, paint);
        }

        for (int i = 1; i < numRows; i++) {
            canvas.drawLine(0, i * cellHeight, width, i * cellHeight, paint);
        }
    }

    public void setMapColors(int homeR, int homeC, int blueR, int blueC, int robotR, int robotC, int foodR, int foodC, int predR, int predC){
        gridColors = new int[numRows][numColumns];
        for(int i = 0; i < numRows; i++){
            for(int j = 0; j < numColumns; j++){
                gridColors[i][j] = 0;
            }
        }

        gridColors[homeR][homeC] = 120; //green
        gridColors[blueR][blueC] = 240; //blue
        gridColors[robotR][robotC] = 60; //yellow
        gridColors[foodR][foodC] = 270; //purple
        gridColors[predR][predC] = 30; //orange
        invalidate();
    }
}
