package abr.colordistance;

import android.util.Log;

import org.opencv.android.OpenCVLoader;
import org.opencv.core.Point;

public class GridCalculator {
    public Point grid_size;
    public Point bottom_left_loc;
    public Point top_right_loc;
    public Double[][] real_locs;

    // called to use OpenCV libraries contained within the app as opposed to a separate download
    static {
        if (!OpenCVLoader.initDebug()) {
            // Handle initialization error
        }
    }

    public GridCalculator(Point gs, Point bll, Point trl){
        grid_size = gs;
        bottom_left_loc = bll;
        top_right_loc = trl;
        real_locs = getRealLocRange();
    }

    public Point convertRealToGrid(Point real_loc){
        int i = 0;
        while(i < (grid_size.x * grid_size.y)){
            if(real_loc.x==real_locs[0][i] && real_loc.y==real_locs[1][i]){
                break;
            }
            i++;
        }
        Point grid_loc = new Point();
        grid_loc.x = i/(int)grid_size.y;
        grid_loc.y = i%(int)grid_size.y;
        return grid_loc;
    }

    public Point findNearestRealLoc(Point orig_real_loc){
        Point real_loc = new Point();
        double minDist = Double.MAX_VALUE;
        double minX = 0;
        double minY = 0;
        for(int i = 0; i < grid_size.x*grid_size.y; i++){
            double dist = Math.sqrt(Math.pow(orig_real_loc.x-real_locs[0][i],2)+Math.pow(orig_real_loc.y-real_locs[1][i],2));
            if(dist < minDist){
                minDist = dist;
                minX = real_locs[0][i];
                minY = real_locs[1][i];
            }
        }
        real_loc.x = minX;
        real_loc.y = minY;
        return real_loc;
    }

    public Double[][] getRealLocRange(){
        double x_space = (top_right_loc.x-bottom_left_loc.x)/(grid_size.x-1);
        double y_space = (top_right_loc.y-bottom_left_loc.y)/(grid_size.y-1);
        Double[][] real_locs = new Double[2][(int)grid_size.x * (int)grid_size.y];
        int k = 0;
        for (int i = 0; i < grid_size.x; i++){
            for (int j = 0; j < grid_size.y; j++) {
                real_locs[0][k] = bottom_left_loc.x + x_space * i;
                real_locs[1][k] = bottom_left_loc.y + y_space * j;
                k++;
            }
        }
        return real_locs;
    }
}
