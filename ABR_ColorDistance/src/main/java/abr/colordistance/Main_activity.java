/**
 * App for FoodPredator experiment
 * Developed by Cognitive Anteater Robotics Laboratory at University of California, Irvine
 * Controls wheeled robot through IOIO
 * Parts of code adapted from OpenCV blob follow
 * Before running, connect phone to IOIO with a bluetooth connection
 * If you would like to uncomment sections for message passing, first connect peer phones using wifi direct
 */
package abr.colordistance;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;

import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import ioio.lib.util.IOIOLooper;
import ioio.lib.util.IOIOLooperProvider;
import ioio.lib.util.android.IOIOAndroidApplicationHelper;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.ToneGenerator;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Main_activity extends Activity implements IOIOLooperProvider, CvCameraViewListener2 // implements IOIOLooperProvider: from IOIOActivity
{
	private final IOIOAndroidApplicationHelper helper_ = new IOIOAndroidApplicationHelper(this, this); // from IOIOActivity
	
	// ioio variables
	IOIO_thread m_ioio_thread;
	
	//blob detection variables
	private CameraBridgeViewBase mOpenCvCameraView;
	private Mat mRgba;
	private ColorBlobDetector mDetectorPredator;
	private ColorBlobDetector mDetectorHome;
	private ColorBlobDetector mDetectorFood;
	private ColorBlobDetector mDetectorBlue;
	private Scalar CONTOUR_COLOR;

	//location variables
	private Long lastRecordedTime;
	private double foodDistance;
	private double foodAngle;
	private double predatorDistance;
	private double predatorAngle;
	private double robotAngle;
	private double estimatedRobotAngle;
	private double homeDistance;
	private double homeAngle;
	private double blueDistance;
	private double blueAngle;
	private Point currLoc;
	private Point estimatedCurrLoc;
	private Point predLoc;
	private Point foodLoc;
	private Point homeLocGrid;
	private Point currLocGrid;
	private Point blueLocGrid;
	private Point foodLocGrid;
	private Point predLocGrid;
	private final double G_x = -60;
	private final double G_y = -60;
	private final double B_x = 280;
	private final double B_y = -60;
	private Point origin;
	private int originRadius;
	private GridCalculator gc;
	private int gridRows = 10;
	private int gridCols = 10;

	private Point centerFood;
	private Point centerPredator;
	private Point centerBlue;
	private Point centerHome;
	private float[] radiusFood;
	private float[] radiusPredator;
	private float[] radiusBlue;
	private float[] radiusHome;

	//agent variables
	private AgentForage af;
	private int t;
	private Context th;

	//ui variables
	TextView sonar1Text;
	TextView sonar2Text;
	TextView sonar3Text;
	TextView energyText;
	TextView homeText;
	TextView foodText;
	TextView predText;
	PixelGridView pixelGrid;
	
	//sockets for message passing
	Boolean isClient = false;
	ServerSocket serverSocket;
	Socket socket;
	Socket clientSocket;
	DataInputStream dataInputStream;
	DataOutputStream dataOutputStream;

	//state variables
	private boolean isDone = false;
	private boolean originIsSet;
	private boolean autoMode;
	private boolean gridMode = true;
	private boolean atHome = false;

	//logging variables
	private String myFilename;
	private FileOutputStream fos;
	private boolean elseFood = false;
	private boolean elseHome = false;
	private boolean elsePred = false;


	//testing condition
	boolean modelBased = true;
	boolean connecting = true;

	// called to use OpenCV libraries contained within the app as opposed to a separate download
	static {
		if (!OpenCVLoader.initDebug()) {
			// Handle initialization error
		}
	}
	
	// called whenever the activity is created
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.main);

		helper_.create(); // from IOIOActivity

		//set up opencv camera
		mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.color_blob_detection_activity_surface_view);
		mOpenCvCameraView.setCvCameraViewListener(this);
		mOpenCvCameraView.enableView();

		//initialize textviews
		sonar1Text = (TextView) findViewById(R.id.sonar1);
		sonar2Text = (TextView) findViewById(R.id.sonar2);
		sonar3Text = (TextView) findViewById(R.id.sonar3);
		energyText = (TextView) findViewById(R.id.energy);
		homeText = (TextView) findViewById(R.id.homeDist);
		foodText = (TextView) findViewById(R.id.foodDist);
		predText = (TextView) findViewById(R.id.predDist);

		//add functionality to gridMode button
		Button buttonGrid = (Button) findViewById(R.id.btnGrid);
		buttonGrid.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (!gridMode) {
					v.setBackgroundResource(R.drawable.button_grid_on);
					gridMode = true;
					pixelGrid.setVisibility(View.VISIBLE);
					Log.i("af","action:"+af.getAgentAction(42,9,0,8,1,8,1));
				} else {
					v.setBackgroundResource(R.drawable.button_grid_off);
					gridMode = false;
					pixelGrid.setVisibility(View.INVISIBLE);
				}
			}
		});

		th = this;

		//add functionality to autoMode button
		Button buttonAuto = (Button) findViewById(R.id.btnAuto);
		buttonAuto.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (!autoMode) {
					v.setBackgroundResource(R.drawable.button_auto_on);
					autoMode = true;

					Calendar calendar = Calendar.getInstance();
					java.util.Date now = calendar.getTime();
					java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
					if(modelBased)
						myFilename = currentTimestamp.toString()+"model" + ".csv";
					else
						myFilename = currentTimestamp.toString()+"random" + ".csv";
					af.setEnergyLevel(3.0);
					isDone = false;
					t = 1;
					Runnable moveThread = new Runnable() {
						public void run() {
							try {
								Thread.sleep(5000);
								if(connecting) {
									sendInt(2);
									ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
									toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_INCALL_LITE, 200);
								}
							} catch (InterruptedException e){
								Log.e("error",e.getMessage());
							}
							while (!isDone) {
								int test = 0;
								if(connecting)
									test = getInt();
								Log.i("haha","test:"+test);
								if (test == 1) {//|| af.distBetween(gridRows - 1 - (int)currLocGrid.y, (int)currLocGrid.x, gridRows - 1 - (int)predLocGrid.y,  (int)predLocGrid.x) < 1) {
									isDone = true;
									MediaPlayer mp = MediaPlayer.create(th, R.raw.smb_mariodie); //caught
									mp.start();
									Log.i("action","caught");
								}
								else if (af.getEnergyLevel() < 0.001) {
									if(connecting)
										sendInt(1);
									isDone = true;
									MediaPlayer mp = MediaPlayer.create(th, R.raw.smb_gameover); //starved
									mp.start();
									Log.i("action","starve");
								}
								else if (foodDistance < 60){//(af.distBetween(gridRows - 1 - (int)currLocGrid.y, (int)currLocGrid.x,  gridRows - 1 - (int)foodLocGrid.y, (int)foodLocGrid.x) < 1) {
									if(connecting)
										sendInt(1);
									isDone = true;
									MediaPlayer mp = MediaPlayer.create(th, R.raw.smb_powerup); //food
									mp.start();
									Log.i("action","food");
								} else {
									int action = 0;
									getMeasurements();
									positionLogNSave(myFilename, t, action, gridRows - 1 - (int)currLocGrid.y, (int)currLocGrid.x, gridRows - 1 - (int)foodLocGrid.y, (int)foodLocGrid.x, gridRows - 1 - (int)predLocGrid.y,  (int)predLocGrid.x, homeDistance, predatorDistance, foodDistance, 0, 0, 0, 0, 0, 0,elseHome,elsePred,elseFood,0);
									if(modelBased)
										action = af.getAgentAction (t, gridRows - 1 - (int)currLocGrid.y, (int)currLocGrid.x, gridRows - 1 - (int)foodLocGrid.y, (int)foodLocGrid.x, gridRows - 1 - (int)predLocGrid.y,  (int)predLocGrid.x)+1;
									else
										action = (int)(Math.random()*3) + 1;
									positionLogNSave(myFilename, t, action, gridRows - 1 - (int)currLocGrid.y, (int)currLocGrid.x, gridRows - 1 - (int)foodLocGrid.y, (int)foodLocGrid.x, gridRows - 1 - (int)predLocGrid.y,  (int)predLocGrid.x, homeDistance, predatorDistance, foodDistance, af.valuesLog[0], af.valuesLog[1], af.valuesLog[2], af.distToNestLog, af.distToPredLog, af.distToFoodLog,elseHome,elsePred,elseFood,af.getEnergyLevel());
									Log.i("action","action:"+action);

									ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
									switch(action){
										case 1: // go to food
											toneG.startTone(ToneGenerator.TONE_CDMA_PIP, 200);
											toneG.release();
											break;
										case 2: // go to nest
											toneG.startTone(ToneGenerator.TONE_CDMA_INTERCEPT, 200);
											toneG.release();
											break;
										default: // run away from predator
											toneG.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT, 200);
											toneG.release();
											break;
									}
									try {
										Thread.sleep(100);
									} catch (Exception e){
										Log.e("error",e.getMessage());
									}
									doAction(action,currLoc,robotAngle);
									++t;
									af.setEnergyLevel(af.calculateEnergy(af.getEnergyLevel(), gridRows - 1 - (int)currLocGrid.y, (int)currLocGrid.x));
								}
							}
						}
					};
					Thread t1 = new Thread(moveThread);
					t1.start();
				} else {
					v.setBackgroundResource(R.drawable.button_auto_off);
					autoMode = false;
					isDone = true;
					if(connecting)
						sendInt(1);
				}
			}
		});

		//setup pixel grid
		pixelGrid = new PixelGridView(this);
		pixelGrid.setNumRows(gridRows);
		pixelGrid.setNumColumns(gridCols);
		pixelGrid.setMapColors(0,0,0,0,0,0,0,0,0,0);
		pixelGrid.setId(View.generateViewId());
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(300, 300);
		lp.setMargins(50, 100, 0, 0);
		pixelGrid.setLayoutParams(lp);
		((RelativeLayout) findViewById(R.id.mainrl)).addView(pixelGrid);

		//set starting autoMode button color
		if (autoMode) {
			buttonAuto.setBackgroundResource(R.drawable.button_auto_on);
		} else {
			buttonAuto.setBackgroundResource(R.drawable.button_auto_off);
		}

		//set up sockets for communication with other robots
		if(connecting) {
			if (isClient) {
				try {
					Object[] objects = (new FileClientAsyncTask()).execute().get();
					socket = (Socket) objects[0];
					dataOutputStream = (DataOutputStream) objects[1];
					dataInputStream = (DataInputStream) objects[2];
				} catch (Exception e) {
					Log.e("rescue robotics", e.getMessage());
				}
			} else {
				try {
					Object[] objects = (new FileServerAsyncTask()).execute().get();
					serverSocket = (ServerSocket) objects[0];
					clientSocket = (Socket) objects[1];
					dataInputStream = (DataInputStream) objects[2];
					dataOutputStream = (DataOutputStream) objects[3];
				} catch (Exception e) {
					Log.e("rescue robotics", e.getMessage());
				}
			}
		}
		gc = new GridCalculator(new Point(gridCols,gridRows), new Point(-30,-30), new Point(240,240));

		mDetectorPredator = new ColorBlobDetector();
		mDetectorFood = new ColorBlobDetector();
		mDetectorHome = new ColorBlobDetector();
		mDetectorBlue = new ColorBlobDetector();

		lastRecordedTime = System.currentTimeMillis();
		currLoc = new Point();
		foodLoc = new Point();
		predLoc = new Point();
		homeLocGrid = new Point();
		currLocGrid = new Point();
		blueLocGrid = new Point();
		foodLocGrid = new Point();
		predLocGrid = new Point();
		origin = new Point(300,300);
		originIsSet = false;

		af = new AgentForage(gridRows,gridCols);
	}

	//Called whenever activity resumes from pause
	@Override
	public void onResume() {
		super.onResume();
	    if (mOpenCvCameraView != null)
			mOpenCvCameraView.enableView();
	}
	
	//Called when activity pauses
	@Override
	public void onPause() {
		super.onPause();
		if (mOpenCvCameraView != null)
			mOpenCvCameraView.disableView();
	}
	
	//Called when activity restarts. onCreate() will then be called
	@Override
	public void onRestart() {
		super.onRestart();
		Log.i("activity cycle","main activity restarting");
	}

	//Called when camera view starts. change bucket color here
	public void onCameraViewStarted(int width, int height) {
		mRgba = new Mat(height, width, CvType.CV_8UC4);
		CONTOUR_COLOR = new Scalar(255, 0, 0, 255);

		//To set color, find HSV values of desired color and convert each value to 1-255 scale
		//mDetector.setHsvColor(new Scalar(7, 196, 144)); // red
		//mDetector.setHsvColor(new Scalar(253.796875,222.6875,195.21875));
		mDetectorFood.setHsvColor(new Scalar(194, 90, 195)); //food purple
		mDetectorPredator.setHsvColor(new Scalar(17, 235, 253)); //predator orange
		mDetectorHome.setHsvColor(new Scalar(95.484375, 213.34375, 181.09375)); // home green
		mDetectorBlue.setHsvColor((new Scalar(155.640625, 181.796875, 185.9375))); // corner blue
	}
	//Called when camera view stops
	public void onCameraViewStopped() {
		mRgba.release();
	}
	//Called at every camera frame. Main controls of the robot movements are in this function
	public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
		mRgba = inputFrame.rgba();

		if(!originIsSet) {
			originIsSet = true;
			setOrigin(mRgba);
		}
		setText("IR1:"+String.format("%.2f", m_ioio_thread.get_ir1_reading()),sonar1Text);
		setText("IR2:"+String.format("%.2f", m_ioio_thread.get_ir2_reading()),sonar2Text);
		setText("IR3:"+String.format("%.2f", m_ioio_thread.get_ir3_reading()),sonar3Text);
		setText("Energy:"+String.format("%.2f", af.getEnergyLevel()),energyText);

		mDetectorFood.process(mRgba);
		mDetectorPredator.process(mRgba);
		mDetectorHome.process(mRgba);
		mDetectorBlue.process(mRgba);
		Imgproc.drawMarker(mRgba,origin,CONTOUR_COLOR);
		Imgproc.drawMarker(mRgba,new Point(origin.x+originRadius,origin.y),CONTOUR_COLOR);
		List<MatOfPoint> contoursFood = mDetectorFood.getContours();
		centerFood = mDetectorFood.getCenter();
		radiusFood = mDetectorFood.getRadius();
		Imgproc.drawContours(mRgba, contoursFood, -1, CONTOUR_COLOR);

		List<MatOfPoint> contoursPredator = mDetectorPredator.getContours();
		centerPredator = mDetectorPredator.getCenter();
		radiusPredator = mDetectorPredator.getRadius();
		Imgproc.drawContours(mRgba, contoursPredator, -1, CONTOUR_COLOR);

		List<MatOfPoint> contoursHome = mDetectorHome.getContours();
		centerHome = mDetectorHome.getCenter();
		radiusHome = mDetectorHome.getRadius();
		Imgproc.drawContours(mRgba, contoursHome, -1, CONTOUR_COLOR);

		List<MatOfPoint> contoursBlue = mDetectorBlue.getContours();
		centerBlue = mDetectorBlue.getCenter();
		radiusBlue = mDetectorBlue.getRadius();
		Imgproc.drawContours(mRgba, contoursBlue, -1, CONTOUR_COLOR);

		/*Log.i("measurement", "Center of circle: (" + center.x + ", " + center.y + "), radius: "
				+ String.format("%.2f", radius[0]));*/
		if(System.currentTimeMillis()-lastRecordedTime > 500 && !autoMode) {
			lastRecordedTime = System.currentTimeMillis();
			getMeasurements();
		}

		return mRgba;
	}

	public void getMeasurements(){
		boolean homeOverlaps = overlaps(centerHome,radiusHome[0],centerPredator,radiusPredator[0]) || overlaps(centerHome,radiusHome[0],centerFood,radiusFood[0]);
		Log.i("help","homeOverlapsWithPred:"+overlaps(centerHome,radiusHome[0],centerPredator,radiusPredator[0]));
		Log.i("help","homeOverlapsWithFood:"+overlaps(centerHome,radiusHome[0],centerFood,radiusFood[0]));
		boolean blueOverlaps = overlaps(centerBlue,radiusBlue[0],centerPredator,radiusPredator[0]) || overlaps(centerBlue,radiusBlue[0],centerFood,radiusFood[0]);
		Log.i("help","blueOverlaps:"+blueOverlaps);

		if (radiusHome[0] > 10 && !homeOverlaps) {
			homeDistance = (3241.1 / Math.pow(radiusHome[0],.858));
			homeAngle = Math.atan2(origin.x-centerHome.x,origin.y-centerHome.y)*180/Math.PI;
			if(homeAngle < 0)
				homeAngle += 180;
			else
				homeAngle -= 180;
			double alpha = homeAngle-90;
			double omega = Math.toDegrees(Math.asin((currLoc.y-G_y)/homeDistance));
			Log.i("measurement","alpha: " + alpha + ",omega: " + omega);
			robotAngle = 180-alpha+omega;
			if(robotAngle < -180)
				robotAngle += 360;
			else if(robotAngle > 180)
				robotAngle -= 360;
			//	robotAngle -= 180;
			Log.i("measurement", "HomeAngle: " + String.format("%.2f", homeAngle) + ", HomeRadius: "
					+ String.format("%.2f", radiusHome[0]) + ", HomeDistance: " + String.format("%.2f", homeDistance) + "cm. ");
			setText("HomeDist: "+String.format("%.2f", homeDistance),homeText);
		} else {
			//estimate home angle and distance, assume home did not move
			homeAngle = Math.atan2(G_y-currLoc.y,G_x-currLoc.x)-robotAngle;
			homeDistance = Math.sqrt(Math.pow(G_y-currLoc.y,2)+Math.pow(G_x-currLoc.x,2));
			Log.i("measurement", "Est HomeAngle:" + String.format("%.2f", homeAngle) + ", Est homeDistance:" + String.format("%.2f", homeDistance));
			setText("HomeDist:*"+String.format("%.2f", homeDistance),homeText);
		}

		if (radiusBlue[0] > 10 && !blueOverlaps) {
			blueDistance = (3241.1 / Math.pow(radiusBlue[0],.858));
			blueAngle = Math.atan2(origin.x-centerBlue.x,origin.y-centerBlue.y)*180/Math.PI;
			if(blueAngle < 0)
				blueAngle += 180;
			else
				blueAngle -= 180;
			double beta = 90 - blueAngle;
			double theta = Math.toDegrees(Math.asin((currLoc.y-B_y)/blueDistance));
			robotAngle = beta - theta;
			Log.i("measurement","theta: " + theta + ",beta: " + beta);
			Log.i("measurement", "BlueAngle: " + String.format("%.2f", blueAngle) + ", BlueRadius: "
					+ String.format("%.2f", radiusBlue[0]) + ", BlueDistance: " + String.format("%.2f", blueDistance) + "cm. ");
		} else {
			//estimate food angle and distance, assume blue did not move
			blueAngle = Math.atan2(currLoc.y-B_y,currLoc.x-B_x) - robotAngle;
			blueDistance = Math.sqrt(Math.pow(B_y-currLoc.y,2)+Math.pow(B_x-currLoc.x,2));
			Log.i("measurement", "Est BlueAngle:" + String.format("%.2f", blueAngle) + ", Est BlueDistance:" + String.format("%.2f", blueDistance));
		}

		if(radiusHome[0] > 10 && radiusBlue[0] > 10 && !homeOverlaps && !blueOverlaps){
			currLoc = triangulate(homeDistance,blueDistance);
			Log.i("triangulate","homeDistance:"+homeDistance+", blueDistance:"+blueDistance);
			Log.i("measurement","Location: " + String.format("%.2f", currLoc.x) + "," + String.format("%.2f", currLoc.y) + ", Orientation: " + String.format("%.2f",robotAngle));
		} else {
			if(estimatedCurrLoc!=null) {
				currLoc = estimatedCurrLoc;
				robotAngle = estimatedRobotAngle;
			}
			Log.i("measurement","Est Location: " + String.format("%.2f", currLoc.x) + "," + String.format("%.2f", currLoc.y) + ", Orientation: " + String.format("%.2f",robotAngle));
		}
		Log.i("curr","curr:"+currLoc.x+","+currLoc.y);

		boolean predFoodOverlaps = overlaps(centerPredator,radiusPredator[0],centerFood,radiusFood[0]);
		Log.i("help","predFoodOverlaps:"+predFoodOverlaps);
		if ((radiusFood[0] > 10 && !predFoodOverlaps) || (radiusFood[0] > radiusPredator[0] && predFoodOverlaps) ) {
			foodDistance = (4001.3 / Math.pow(radiusFood[0],.908));
			foodAngle = Math.atan2(origin.x-centerFood.x,origin.y-centerFood.y)*180/Math.PI;
			if(foodAngle < 0)
				foodAngle += 180;
			else
				foodAngle -= 180;
			double foodAngleRelRobot = robotAngle - (90 - foodAngle);
			if(foodAngleRelRobot > 180)
				foodAngleRelRobot -= 360;
			else if(foodAngleRelRobot < -180)
				foodAngleRelRobot += 360;
			foodLoc.x = foodDistance * Math.cos(Math.toRadians(foodAngleRelRobot)) + currLoc.x;
			foodLoc.y = foodDistance * Math.sin(Math.toRadians(foodAngleRelRobot)) + currLoc.y;
			setText("FoodDist: "+String.format("%.2f", foodDistance),foodText);
			Log.i("measurement", "FoodAngle: " + String.format("%.2f", foodAngle) + ", FoodRadius: "
					+ String.format("%.2f", radiusFood[0]) + ", FoodDistance: " + String.format("%.2f", foodDistance) + "cm, FoodLoc:" + String.format("%.2f", foodLoc.x) + "," + String.format("%.2f", foodLoc.y));
		} else {
			//estimate food angle and distance, assume predator did not move
			foodAngle = Math.atan2(foodLoc.y-currLoc.y,foodLoc.x-currLoc.x);
			foodDistance = Math.sqrt(Math.pow(foodLoc.y-currLoc.y,2)+Math.pow(foodLoc.x-currLoc.x,2));
			setText("FoodDist:*"+String.format("%.2f", foodDistance),foodText);
			Log.i("measurement", "Est FoodAngle:" + String.format("%.2f", foodAngle) + ", Est FoodDistance:" + String.format("%.2f", foodDistance));
		}

		if ((radiusPredator[0] > 10 && !predFoodOverlaps) || (radiusPredator[0] > radiusFood[0] && predFoodOverlaps) ) {
			//predatorDistance = (3141.1 / Math.pow(radiusFood[0],0.895)); //power function, small balloon
			predatorDistance = -94.94*Math.log(radiusPredator[0])+483.11;
			predatorAngle = Math.atan2(origin.x-centerPredator.x,origin.y-centerPredator.y)*180/Math.PI;
			if(predatorAngle < 0)
				predatorAngle += 180;
			else
				predatorAngle -= 180;
			double predAngleRelRobot = robotAngle - (90 - predatorAngle);
			if(predAngleRelRobot > 180)
				predAngleRelRobot -= 360;
			else if(predAngleRelRobot < -180)
				predAngleRelRobot += 360;
			predLoc.x = predatorDistance * Math.cos(Math.toRadians(predAngleRelRobot)) + currLoc.x;
			predLoc.y = predatorDistance * Math.sin(Math.toRadians(predAngleRelRobot)) + currLoc.y;
			setText("PredDist: "+String.format("%.2f", predatorDistance),predText);
			Log.i("measurement", "PredatorAngle: " + String.format("%.2f", predatorAngle) + ", PredatorRadius: "
					+ String.format("%.2f", radiusPredator[0]) + ", PredatorDistance: " + String.format("%.2f", predatorDistance) + "cm, predLoc: " + String.format("%.2f", predLoc.x) + "," + String.format("%.2f", predLoc.y));
		} else {
			//estimate pred angle and distance, assume food did not move
			Log.i("measurement", "No predator detected");
			predatorAngle = Math.atan2(foodLoc.y-currLoc.y,foodLoc.x-currLoc.x);
			predatorDistance = Math.sqrt(Math.pow(foodLoc.y-currLoc.y,2)+Math.pow(foodLoc.x-currLoc.x,2));
			setText("PredDist:*"+String.format("%.2f", predatorDistance),predText);
			Log.i("measurement", "Est PredatorAngle:" + String.format("%.2f", foodAngle) + ", Est PredatorDistance:" + String.format("%.2f", foodDistance));
		}

		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				homeLocGrid = gc.findNearestRealLoc(new Point(G_x,G_y));
				currLocGrid = gc.findNearestRealLoc(currLoc);
				blueLocGrid = gc.findNearestRealLoc(new Point(B_x,B_y));
				foodLocGrid = gc.findNearestRealLoc(foodLoc);
				predLocGrid = gc.findNearestRealLoc(predLoc);
				//Log.i("foop","food1:"+(int)foodLocGrid.x+","+(int)foodLocGrid.y);
				//Log.i("foop","pred1:"+(int)predLocGrid.x+","+(int)predLocGrid.y);
				//Log.i("foop","home1:"+(int)homeLocGrid.x+","+(int)homeLocGrid.y);
				//Log.i("foop","curr1:"+(int)currLocGrid.x+","+(int)currLocGrid.y);
				//Log.i("foop","blue1:"+(int)blueLocGrid.x+","+(int)blueLocGrid.y);

				homeLocGrid = gc.convertRealToGrid(homeLocGrid);
				currLocGrid = gc.convertRealToGrid(currLocGrid);
				blueLocGrid = gc.convertRealToGrid(blueLocGrid);
				foodLocGrid = gc.convertRealToGrid(foodLocGrid);
				predLocGrid = gc.convertRealToGrid(predLocGrid);

				//Log.i("foop","home:"+(gridRows - 1 - (int)homeLocGrid.y)+","+(int)homeLocGrid.x);
				//Log.i("foop","blue:"+(gridRows - 1 - (int)blueLocGrid.y)+","+(int)blueLocGrid.x);
				//Log.i("foop","curr:"+(gridRows - 1 - (int)currLocGrid.y)+","+(int)currLocGrid.x);
				//Log.i("foop","food:"+(gridRows - 1 - (int)foodLocGrid.y)+","+(int)foodLocGrid.x);
				//Log.i("foop","pred:"+(gridRows - 1 - (int)predLocGrid.y)+","+(int)predLocGrid.x);
				pixelGrid.setMapColors(
						gridRows - 1 - (int)homeLocGrid.y, (int)homeLocGrid.x,
						gridRows - 1 - (int)blueLocGrid.y, (int)blueLocGrid.x,
						gridRows - 1 - (int)currLocGrid.y, (int)currLocGrid.x,
						gridRows - 1 - (int)foodLocGrid.y, (int)foodLocGrid.x,
						gridRows - 1 - (int)predLocGrid.y, (int)predLocGrid.x);
			}
		});
	}

	public void doAction(int act, Point startLoc, double startAngle){
		double angle_to_turn = 0;
		double extra_turn = 0;
		long startTime = System.currentTimeMillis();
		while(System.currentTimeMillis() < (startTime + 1000));
		switch (act) {
			case 1: // go to food
				angle_to_turn = foodAngle - 90;
				if (angle_to_turn > 180) {angle_to_turn -= 360;}
				else if (angle_to_turn < -180) {angle_to_turn += 360;}
				if (angle_to_turn < 0) {
					rotateOrMove("right",1750,Math.abs(angle_to_turn));
					m_ioio_thread.turn(1500);
				} else {
					rotateOrMove("left",1200,Math.abs(angle_to_turn));
					m_ioio_thread.turn(1500);
				}
				extra_turn = 0;
				if(m_ioio_thread.get_ir1_reading() <= 18 || m_ioio_thread.get_ir2_reading() <= 18 || m_ioio_thread.get_ir3_reading() <=18){
					if(m_ioio_thread.get_ir1_reading() <= 18 && m_ioio_thread.get_ir3_reading() <= 18) {
						angle_to_turn += 180;
						extra_turn = 180;
					}
					else if(m_ioio_thread.get_ir1_reading() < m_ioio_thread.get_ir3_reading()) {
						angle_to_turn -= 90;
						extra_turn = -90;
					}
					else {
						angle_to_turn += 90;
						extra_turn = 90;
					}
				}
				if (angle_to_turn > 180) {angle_to_turn -= 360;}
				else if (angle_to_turn < -180) {angle_to_turn += 360;}
				if (extra_turn < 0) {
					rotateOrMove("right",1750,Math.abs(extra_turn));
					m_ioio_thread.turn(1500);
				} else if (extra_turn > 0) {
					rotateOrMove("left",1200,Math.abs(extra_turn));
					m_ioio_thread.turn(1500);
				}
				//estimate new robot angle
				estimatedRobotAngle = startAngle + angle_to_turn;
				if (estimatedRobotAngle > 180) {estimatedRobotAngle -= 360;}
				else if (estimatedRobotAngle < -180) {estimatedRobotAngle += 360;}
				if(foodDistance >= 60 && m_ioio_thread.get_ir2_reading() > 18) {
					rotateOrMove("forward", 1750, 20);
					m_ioio_thread.move(1500);
					//estimate new location
					double headingAngle = estimatedRobotAngle + 90;
					estimatedCurrLoc = new Point(startLoc.x + 20 * Math.cos(Math.toRadians(headingAngle)), startLoc.y + 20 * Math.sin(Math.toRadians(headingAngle)));
					elseFood = false;
				} else {
					estimatedCurrLoc = new Point(startLoc.x,startLoc.y);
					elseFood = true;
				}
				break;
			case 2: // go home
				angle_to_turn = homeAngle - 90;
				if (angle_to_turn > 180) {angle_to_turn -= 360;}
				else if (angle_to_turn < -180) {angle_to_turn += 360;}
				if (angle_to_turn < 0) {
					rotateOrMove("right",1750,Math.abs(angle_to_turn));
					m_ioio_thread.turn(1500);
				} else {
					rotateOrMove("left",1200,Math.abs(angle_to_turn));
					m_ioio_thread.turn(1500);
				}
				extra_turn = 0;
				if(m_ioio_thread.get_ir1_reading() <= 18 || m_ioio_thread.get_ir2_reading() <= 18 || m_ioio_thread.get_ir3_reading() <=18){
					if(m_ioio_thread.get_ir1_reading() <= 18 && m_ioio_thread.get_ir3_reading() <= 18) {
						angle_to_turn += 180;
						extra_turn = 180;
					}
					else if(m_ioio_thread.get_ir1_reading() < m_ioio_thread.get_ir3_reading()) {
						angle_to_turn -= 90;
						extra_turn = -90;
					}
					else {
						angle_to_turn += 90;
						extra_turn = 90;
					}
				}
				if (angle_to_turn > 180) {angle_to_turn -= 360;}
				else if (angle_to_turn < -180) {angle_to_turn += 360;}
				if (extra_turn < 0) {
					rotateOrMove("right",1750,Math.abs(extra_turn));
					m_ioio_thread.turn(1500);
				} else if (extra_turn > 0) {
					rotateOrMove("left",1200,Math.abs(extra_turn));
					m_ioio_thread.turn(1500);
				}
				//estimate new robot angle
				estimatedRobotAngle = startAngle + angle_to_turn;
				if (estimatedRobotAngle > 180) {estimatedRobotAngle -= 360;}
				else if (estimatedRobotAngle < -180) {estimatedRobotAngle += 360;}
				if(homeDistance >= 60 && m_ioio_thread.get_ir2_reading() > 18) {
					rotateOrMove("forward", 1750, 20);
					//estimate new location
					double headingAngle = estimatedRobotAngle + 90;
					estimatedCurrLoc = new Point(startLoc.x + 20 * Math.cos(Math.toRadians(headingAngle)), startLoc.y + 20 * Math.sin(Math.toRadians(headingAngle)));
					elseHome = false;
				} else {
					estimatedCurrLoc = new Point(startLoc.x,startLoc.y);
					elseHome = true;
				}
				break;
			case 3: // go to predator
				angle_to_turn = predatorAngle - (-90);
				if (angle_to_turn > 180) {angle_to_turn -= 360;}
				else if (angle_to_turn < -180) {angle_to_turn += 360;}
				if (angle_to_turn < 0) {
					rotateOrMove("right",1750,Math.abs(angle_to_turn));
					m_ioio_thread.turn(1500);
				} else {
					rotateOrMove("left",1200,Math.abs(angle_to_turn));
					m_ioio_thread.turn(1500);
				}
				extra_turn = 0;
				if(m_ioio_thread.get_ir1_reading() <= 15 || m_ioio_thread.get_ir2_reading() <= 15 || m_ioio_thread.get_ir3_reading() <=15){
					if(m_ioio_thread.get_ir1_reading() <= 15 && m_ioio_thread.get_ir3_reading() <= 15) {
						angle_to_turn += 180;
						extra_turn = 180;
					}
					else if(m_ioio_thread.get_ir1_reading() < m_ioio_thread.get_ir3_reading()) {
						angle_to_turn -= 90;
						extra_turn = -90;
					}
					else {
						angle_to_turn += 90;
						extra_turn = 90;
					}
				}
				if (angle_to_turn > 180) {angle_to_turn -= 360;}
				else if (angle_to_turn < -180) {angle_to_turn += 360;}
				if (extra_turn < 0) {
					rotateOrMove("right",1750,Math.abs(extra_turn));
					m_ioio_thread.turn(1500);
				} else if (extra_turn > 0) {
					rotateOrMove("left",1200,Math.abs(extra_turn));
					m_ioio_thread.turn(1500);
				}
				//estimate new robot angle
				estimatedRobotAngle = startAngle + angle_to_turn;
				if (estimatedRobotAngle > 180) {estimatedRobotAngle -= 360;}
				else if (estimatedRobotAngle < -180) {estimatedRobotAngle += 360;}
				if(predatorDistance > 60 && m_ioio_thread.get_ir2_reading() > 18) {
					rotateOrMove("forward", 1700, 20);
					//estimate new location
					double headingAngle = estimatedRobotAngle + 90;
					estimatedCurrLoc = new Point(startLoc.x + 20 * Math.cos(Math.toRadians(headingAngle)), startLoc.y + 20 * Math.sin(Math.toRadians(headingAngle)));
					elsePred = false;
				} else {
					estimatedCurrLoc = new Point(startLoc.x,startLoc.y);
					elsePred = true;
				}
				break;
		}
		m_ioio_thread.move(1500);
		if(homeDistance < 120 && !atHome){
			atHome = true;
			if(connecting)
				sendInt(3);
			ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
			toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_INCALL_LITE, 200);
			Log.i("blah","int sent");
		} else if (homeDistance >= 120 && atHome) {
			atHome = false;
			if(connecting)
				sendInt(4);
			ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
			toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_INCALL_LITE, 200);
			Log.i("blah","int sent");
		} else {
			Log.i("blah","atHome:"+atHome + ",homeDistance:" + homeDistance);
		}
	}

	//send an integer using output stream from socket
	public void sendInt(int intToSend){
		if(dataOutputStream != null) {
			try {
				Log.i("rescue robotics", "before grid sent");
				dataOutputStream.writeInt(intToSend);
				Log.i("rescue robotics", "grid sent");
			} catch (IOException e) {
				Log.e("rescue robotics", e.getMessage());
			}
		} else {
			Log.i("rescue robotics", "null");
		}
	}

	//receive an integer using input stream from socket
	public int getInt(){
		try {
			if(dataInputStream != null && dataInputStream.available() >= 4) {
				return dataInputStream.readInt();
			}
		} catch (IOException e) {
			Log.e("rescue robotics", e.getMessage());
		}
		return 0;
	}

	//set the text of any text view in this application
	public void setText(final String str, final TextView tv) 
	{
		  runOnUiThread(new Runnable() {
			  @Override
			  public void run() {
				  tv.setText(str);
			  }
		  });
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.i("activity cycle","main activity being destroyed");
		helper_.destroy();
		if (mOpenCvCameraView != null)
			mOpenCvCameraView.disableView();
		if (mOpenCvCameraView != null)
			mOpenCvCameraView.disableView();
	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.i("activity cycle","main activity starting");
		helper_.start();
	}

	@Override
	protected void onStop() {
		Log.i("activity cycle","main activity stopping");
		super.onStop();
		helper_.stop();
		try {
			if(socket != null)
				socket.close();
			if(serverSocket != null)
				serverSocket.close();
			if(clientSocket != null)
				clientSocket.close();
		} catch (IOException e) {
			Log.e("rescue robotics", e.getMessage());
		}
		
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
			if ((intent.getFlags() & Intent.FLAG_ACTIVITY_NEW_TASK) != 0) {
			helper_.restart();
		}
	}

	public void rotateOrMove(String action, int speed, double amount){
		double encoderValue;
		boolean toTurn;
		if (action.equals("left")){
			encoderValue = 518 / 360.0;  // 525 / 360.0 // 490 / 360.0
			toTurn = true;
		} else if (action.equals("right")) {
			encoderValue = 450 / 360.0;  // 454 / 360.0
			toTurn = true;
		} else if (action.equals("forward") || action.equals("backward")) {
			encoderValue = 4.46;
			toTurn = false;
		} else {
			encoderValue = 0;
			toTurn = false;
		}
		m_ioio_thread.counter_left = 0;
		while (m_ioio_thread.counter_left < (int)(encoderValue * amount) && !isDone) {
			if (toTurn){
				m_ioio_thread.turn(speed);
			} else {
				m_ioio_thread.move(speed);
			}
		}
	}

	public Point triangulate(double D_g, double D_b) {
		double C = calculateC(G_x, G_y, B_x, B_y, D_g, D_b);
		double k = calculateK(G_x, G_y, B_x, B_y);

		double m = Math.pow(k,2) + 1;
		double n = 2 * (k * G_x - C * k - G_y);
		double p = Math.pow(C,2) - 2 * G_x * C + Math.pow(G_x,2) + Math.pow(G_y,2) - Math.pow(D_g,2);

		double delta = calculateDelta(m, n, p);

		double R_y_plus = (-n + Math.sqrt(delta)) / (2 * m);
		double R_y_minus = (-n - Math.sqrt(delta)) / (2 * m);

		double R_x_plus = C - k * R_y_plus;
		double R_x_minus = C - k * R_y_minus;

		double x_min = -60;
		double x_max = 500;
		double y_min = -60;
		double y_max = 500;
		Double R_x_selected;
		Double R_y_selected;
		if (R_x_plus >= x_min && R_x_plus <= x_max && R_y_plus >= y_min && R_y_plus <= y_max) {
			R_x_selected = R_x_plus;
			R_y_selected = R_y_plus;
		} else if((R_x_minus >= x_min && R_x_minus <= x_max && R_y_minus >= y_min && R_y_minus <= y_max)) {
			R_x_selected = R_x_minus;
			R_y_selected = R_y_minus;
		} else {
			R_x_selected = 120.0;
			R_y_selected = 0.0;
		}

		return new Point(R_x_selected,R_y_selected);
	}
	public double calculateC(double G_x, double G_y, double B_x, double B_y, double D_g, double D_b) {
		return (Math.pow(G_x,2) + Math.pow(G_y,2) - Math.pow(B_x,2) - Math.pow(B_y,2) + Math.pow(D_b,2) - Math.pow(D_g,2)) / (2 * (G_x - B_x));
	}

	public double calculateK(double G_x, double G_y, double B_x, double B_y) {
		return (B_y - G_y) / (B_x - G_x);
	}

	public double calculateDelta(double m,double n, double p) {
		return Math.pow(n,2) - 4 * m * p;
	}

	public void setOrigin(Mat mRgba){
		Mat bw = new Mat();
		Imgproc.cvtColor(mRgba,bw,Imgproc.COLOR_RGB2GRAY);

		Mat circles = new Mat();
		Vector<Mat> circlesList = new Vector<Mat>();
		Imgproc.HoughCircles(bw, circles, Imgproc.CV_HOUGH_GRADIENT, 1, 60, 150, 60, 30, 200);

		for( int i = 0; i < circles.rows(); i++ )
		{
			double[] data = circles.get(i, 0);
			for(int j = 0 ; j < data.length ; j++){
				origin.x = data[0];
				origin.y = data[1];
				originRadius = (int) data[2];
				Log.i("circ","data:"+data[0]+","+data[1]+","+data[2]);
			}
			mDetectorBlue.setOrigin(origin.x,origin.y);
			mDetectorFood.setOrigin(origin.x,origin.y);
			mDetectorHome.setOrigin(origin.x,origin.y);
			mDetectorPredator.setOrigin(origin.x,origin.y);
		}
	}

	public boolean overlaps(Point center1, double radius1, Point center2, double radius2){
		if (radius1 <= 0 || radius2 <= 0)
			return false;
		double dist = Math.sqrt(Math.pow(center1.x-center2.x,2)+Math.pow(center1.y-center2.y,2));
		if (radius1+radius2 > dist) {
			return true;
		} else {
			return false;
		}
	}

	// to log the current robot position along with the time stamp and append updated info to a file
	public void positionLogNSave(String filename, int t, int act, int agentRow, int agentCol, int foodRow, int foodCol, int predRow, int predCol, double homeDist, double predDist, double foodDist, double v1, double v2, double v3, int distNest, int distPred, int distFood, boolean elseHome, boolean elsePred, boolean elseFood, double energyLevel) {
		long currentTimeMS = System.currentTimeMillis();
		String time = Long.toString(currentTimeMS);
		String naming = "time,timestep,action,agentR,agentC,foodR,foodC,predR,predC,homeDist,foodDist,predDist,v1,v2,v3,distNest,distPred,distFood,elseHome,elsePred,elseFood,energyLevel\n";
		String info = time+","+t+","+act+","+agentRow+","+agentCol+","+foodRow+","+foodCol+","+predRow+","+predCol+","
				+String.format("%.2f",homeDist)+","+String.format("%.2f",predDist)+","+String.format("%.2f",foodDist)
				+ ","+String.format("%.2f",v1)+ ","+String.format("%.2f",v2)+ ","+String.format("%.2f",v3)+ ","
				+ distNest + "," + distPred + "," + distFood + "," + elseHome + "," + elsePred + "," + elseFood + "," + String.format("%.2f",energyLevel) + "\n";
		try {
			File root = new File(Environment.getExternalStorageDirectory(), "FoodPredator");
			if (!root.exists()) {
				root.mkdirs();
			}
			try {
				File file = new File(root, filename);
				if (!file.exists()) {
					file.createNewFile();
					fos=new FileOutputStream(file,true); // make sure the mode allows appending material to the file
					byte[] n = naming.getBytes();
					fos.write(n);
				}
				try {
					byte[] b = info.getBytes();
					fos.write(b);
					if (!autoMode && file.exists()){
						fos.close();
					}
				} catch (IOException e) {
					Log.e("app.main","Couldn't write to SD");
				}
			} catch (Exception ex) {
				Log.e("app.main","Couldn't write to SD");
			}
		} catch (Exception e) {
			Log.e("app.main","Couldn't write to SD");
		}
		Log.i("Saved",info);
	}

	/****************************************************** functions from IOIOActivity *********************************************************************************/
	@Override
	public IOIOLooper createIOIOLooper(String connectionType, Object extra) {
		if (m_ioio_thread == null
				&& connectionType
				.matches("ioio.lib.android.bluetooth.BluetoothIOIOConnection")) {
			m_ioio_thread = new IOIO_thread();
			return m_ioio_thread;
		} else
			return null;
	}
	
}
