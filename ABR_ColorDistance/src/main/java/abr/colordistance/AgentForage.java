/**
 * * **********************************************************************
 *
 * Copyright (c) 2014 Regents of the University of California. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. The names of its contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * **********************************************************************
 * Class agentForage is a model based algorithm. 
 * The agent looks for food, avoids a predator, and can hide in its nest. 
 * The agent uses mental simulation to decide what action to take.
 *
 * @author Jeff Krichmar, Dept of Cognitive Sciences, UC Irvine
 * @version August 2017
 */

package abr.colordistance;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class AgentForage
{
    // instance variables - replace the example below with your own
    private int x;
    private int mapRow;
    private int mapCol;
    private int[] agent;
    private int[] food;
    private int[] pred;
    private double energyLevel;
    private int numStates;
    private int numActions;
    private double[][][] Q;
    private boolean[][][] visitQ;
    private static final double BETA = 5.0f;
    private static final double FOOD = 5.0f;
    private static final double CAUGHT = -5.0f;
    private static final double STARVE = -5.0f;
    private static final double STARVE_THRESHOLD = 0.001;
    private static final double ENERGY_EXPENDITURE_RATE = 0.10f;
    private static final double GAMMA = 0.99f;
    private static final int FIND_FOOD = 0;
    private static final int FIND_NEST = 1;
    private static final int AVOID_PRED = 2;
    private static final int NEST_ROW = 9;
    private static final int NEST_COL = 0;
    private static final int NEST_SAFETY = 4;
    private static final int PREDATOR_SPEED = 2;
    private static final int PREDATOR_VISION = 6;
    private static final int NUM_DIRECTIONS = 8;

    public double[] valuesLog;
    public int distToFoodLog;
    public int distToPredLog;
    public int distToNestLog;

    private FileOutputStream fos1;
    /**
     * Constructor for objects of class agentForage
     *
     * Sets up the grid and state table
     *
     * @param numRows - number of rows in the grid map
     * @param numCols - number of coloumns in the grid map
     */
    public AgentForage(int numRows, int numCols)
    {
        // initialise instance variables
        agent = new int[2];
        food = new int[2];
        pred = new int[2];
        mapRow = numRows;
        mapCol = numCols;

        numActions = 3;
        numStates = distBetween (0, 0, mapRow-1, mapCol-1)+2;
        Q = new double[numStates][numStates][numStates]; // state table is dist2food, dist2nest, dist2pred
        visitQ = new boolean[numStates][numStates][numStates]; // state table is dist2food, dist2nest, dist2pred, number of actions
        energyLevel = 3.0; //FOOD

        for (int i = 0; i < numStates; i++) {
            for (int j = 0; j < numStates; j++) {
                for (int k = 0; k < numStates; k++) {
                    Q[i][j][k] = 1.0f/(double)numActions;
                    visitQ[i][j][k] = false;
                }
            }
        }
        valuesLog = new double[3];
    }

    /****
     * getAgentAction - takes the distances to objects as inputs and calls a function calcQ that implements the Bellman equation. 
     *                  It uses the values generated from that function to select an action.
     *
     * @param agentRow is the row position of the agent in grid space
     * @param agentCol is the column position of the agent in grid space
     * @param foodRow is the row position of the food in grid space
     * @param foodCol is the column position of the food in grid space
     * @param predRow is the row position of the predator in grid space
     * @param predCol is the column position of the predator in grid space
     * @return an action to 1) move to food, 2) move to nest, 3) avoid the predator 
     */
    public int getAgentAction (int t, int agentRow, int agentCol, int foodRow, int foodCol, int predRow, int predCol) {

        double [] v = new double [numActions];

        for (int i = 0; i < numStates; i++) {
            for (int j = 0; j < numStates; j++) {
                for (int k = 0; k < numStates; k++) {
                    visitQ[i][j][k] = false;
                }
            }
        }

        for (int i = 0; i < numActions; i++) {
            v[i] = calcQ (i, t, agentRow, agentCol, foodRow, foodCol, predRow, predCol, energyLevel);
        }
        valuesLog[0] = v[0];
        valuesLog[1] = v[1];
        valuesLog[2] = v[2];
//         System.out.println("V = (" + v[0] + "," + v[1] + "," + v[2] + ")");
        return actionSelect (v, BETA);
    }

    /****
     * calcQ - uses the Bellman equation to predict values of an action. This is a recursive function
     *
     * @param action to take can be 1) move to food, 2) move to nest, 3) avoid the predator
     * @param t counter used for moving the predator
     * @param agentRow is the row position of the agent in grid space
     * @param agentCol is the column position of the agent in grid space
     * @param foodRow is the row position of the food in grid space
     * @param foodCol is the column position of the food in grid space
     * @param predRow is the row position of the predator in grid space
     * @param predCol is the column position of the predator in grid space
     * @param energy is the current energy level of the agent
     * @return returns an expected value for taking that action
     */
    public double calcQ (int action, int t, int agentRow, int agentCol, int foodRow, int foodCol, int predRow, int predCol, double energy) {
        int [] agent = new int[2];
        int [] predator = new int[2];
        double [] qTmp = new double [numActions];
        int distToFood, distToNest, distToPred;
        double qReturn;
        double sumP;
        double maxVal;

        //agent = moveAgent(action, agentRow, agentCol, foodRow, foodCol, predRow, predCol);
        //predator = movePredator(t, agent[0], agent[1], predRow, predCol);
        //energy = calculateEnergy (energy, agent[0], agent[1]);
        //distToFood = distBetween(agent[0], agent[1], foodRow, foodCol);
        //distToNest = distBetween(agent[0], agent[1], NEST_ROW, NEST_ROW);
        //distToPred = distBetween(agent[0], agent[1], predator[0], predator[1]);

        agent = moveAgent(action, agentRow, agentCol, foodRow, foodCol, predRow, predCol);
        predator = movePredator(t, agent[0], agent[1], predRow, predCol);
        energy = calculateEnergy (energy, agent[0], agent[1]);
        distToFood = distBetween(agent[0], agent[1], foodRow, foodCol);
        distToNest = distBetween(agent[0], agent[1], NEST_ROW, NEST_COL);
        distToPred = distBetween(agent[0], agent[1], predator[0], predator[1]);

        if(distToFood >= numStates)
            distToFood = numStates - 1;
        if(distToNest >= numStates)
            distToNest = numStates - 1;
        if(distToPred >= numStates)
            distToNest = numStates - 1;

        distToFoodLog = distToFood;
        distToNestLog = distToNest;
        distToPredLog = distToPred;

        //positionLogNSave("dists2.csv",distToNestLog,distToPredLog,distToFoodLog);

        if (!visitQ [distToFood][distToNest][distToPred]) {
            if (distToPred < 1) {
                qReturn = CAUGHT;
            }
            else if (energy < STARVE_THRESHOLD) {
                qReturn = STARVE;
            }
            else if (distToFood < 1) {
                qReturn = calculateReward (energy);
            }
            else {
                for (int i = 0; i < numActions; i++) {
                    qTmp[i] = GAMMA*calcQ(i, t+1, agent[0], agent[1], foodRow, foodCol, predator[0], predator[1], energy);

                }
                sumP = 0.0f;
                for (int i = 0; i < numActions; i++) {
                    sumP += Math.exp(BETA*qTmp[i]);
                }
                maxVal = -Double.MAX_VALUE;
                for (int i = 0; i < numActions; i++) {
                    if (qTmp[i] > maxVal) {
                        maxVal = qTmp[i];
                    }
                }
                qReturn = (Math.exp(BETA*maxVal)/sumP)*maxVal;
            }
            visitQ [distToFood][distToNest][distToPred] = true;
            Q[distToFood][distToNest][distToPred] = qReturn;
        }
        else {
            qReturn = Q[distToFood][distToNest][distToPred];
        }

        return qReturn;
    }

    /****
     * actionSelect - uses a softmax function to select an action from an array of values
     *
     * @param v is the value array
     * @param beta is the temperature for the softmax function
     * @return index of the selected action from value array
     */
    public int actionSelect(double[] v, double beta) {
        double [] p = new double[v.length];
        double sumP = 0.0f;
        double r = Math.random();
        boolean done = false;
        int inx = 0;
        int aReturn = 0;

        /*
        double maxVal = Double.MIN_VALUE;
        for(int i = 0; i < v.length; i++){
            if(v[i] > maxVal){
                maxVal = v[i];
                aReturn = i;
            }
        }
        */


        for (int i = 0; i < v.length; i++) {
            sumP += Math.exp(BETA*v[i]);
        }
        for (int i = 0; i < p.length; i++) {
            p[i] = Math.exp(BETA*v[i])/sumP;
        }
        sumP = 0.0f;
        while (!done && inx < p.length) {
            sumP = sumP + p[inx];
            if (sumP > r) {
                aReturn = inx;
                done = true;
            }
            ++inx;
        }

        return aReturn;
    }

    /****
     *
     * moveAgent - moves the agent based on the action
     *
     * @param action to be taken
     * @param prevAgentRow previous row position of agent
     * @param prevAgentCol previous column position of agent
     * @param foodRow row position of food source
     * @param foodCol column position of food source     
     * @param predRow row position of predator
     * @param predCol column position of predator
     * @return agent's new position (row and column)
     */
    public int[] moveAgent (int action, int prevAgentRow, int prevAgentCol, int foodRow, int foodCol, int predRow, int predCol) {

        int [] agent = new int[2];
        switch (action) {
            case FIND_FOOD:
                agent = moveTo(prevAgentRow, prevAgentCol, foodRow, foodCol);
                break;
            case FIND_NEST:
                agent = moveTo(prevAgentRow, prevAgentCol, NEST_ROW, NEST_COL);
                break;
            case AVOID_PRED:
                agent = moveFrom (prevAgentRow, prevAgentCol, predRow, predCol);
                break;
        }
        return agent;
    }

    /****
     *
     * movePredator -   moves the predator toward the agent if it sees it. If the predator doesn't see the agent,
     *              if moves randomly. The predator cannot go near the nest.
     *
     * @param cnt counter to control the predator speed
     * @return agent's new position (row and column)
     */
    public int[] movePredator (int cnt, int agentRow, int agentCol, int prevPredRow, int prevPredCol) {

        int [] predator = new int[2];
        int dist2Agent = distBetween(prevPredRow, prevPredCol, agentRow, agentCol);
        int dist2Nest = distBetween(prevPredRow, prevPredCol, NEST_ROW, NEST_COL);

        if (cnt % PREDATOR_SPEED == 0) {
            if (dist2Nest >= NEST_SAFETY && dist2Agent < PREDATOR_VISION) {
                predator = moveTo(prevPredRow, prevPredCol, agentRow, agentCol);
            }
            else if (dist2Nest < NEST_SAFETY) {
                predator = moveFrom (prevPredRow, prevPredCol, NEST_ROW, NEST_COL);
            }
            else {
                predator = move((int)Math.random()*NUM_DIRECTIONS+1, prevPredRow, prevPredCol);
            }
        }
        else {
            predator[0] = prevPredRow;
            predator[1] = prevPredCol;
        }
        return predator;
    }

    /****
     *
     * moveTo - moves the object closer to a location. 
     *
     * @param currRow current row
     * @param currCol current column
     * @param toRow destination row
     * @param toCol destination column
     * @return new position (row and column)
     */
    public int[] moveTo (int currRow, int currCol, int toRow, int toCol) {

        int [] m = new int[2];
        double minDistance = Double.MAX_VALUE;
        int minInx = 0;
        double d;

        for (int i = 0; i < NUM_DIRECTIONS; i++) {
            m = move(i, currRow, currCol);
            d = Math.sqrt(Math.pow(m[0]-toRow,2.0) + Math.pow(m[1]-toCol,2.0));
            if (d < minDistance) {
                minDistance = d;
                minInx = i;
            }
        }
        m = move(minInx, currRow, currCol);
        return m;
    }

    /****
     *
     * moveFrom - moves the object away from a location. 
     *
     * @param currRow current row
     * @param currCol current column
     * @param toRow destination row
     * @param toCol destination column
     * @return new position (row and column)
     */
    public int[] moveFrom (int currRow, int currCol, int toRow, int toCol) {

        int [] m = new int[2];
        double maxDistance = -Double.MAX_VALUE;
        int maxInx = 0;
        double d;

        for (int i = 0; i < NUM_DIRECTIONS; i++) {
            m = move(i, currRow, currCol);
            d = Math.sqrt(Math.pow(m[0]-toRow,2.0) + Math.pow(m[1]-toCol,2.0));
            if (d > maxDistance) {
                maxDistance = d;
                maxInx = i;
            }
        }
        m = move(maxInx, currRow, currCol);
        return m;
    }

    /****
     *
     * move -   moves the object in a given direction. Checks for boundary conditions. Assume that there are 8
     *          directions. The zero direction is North.  Proceeds clockwise from North to Northwest
     *
     * @param dir direction to move
     * @param currRow current row
     * @param currCol current column
     */
    public int[] move (int dir, int currRow, int currCol) {

        int [] m = new int[2];

        m[0] = currRow;
        m[1] = currCol;

        switch (dir) {
            case 0:
                ++m[1];
                break;
            case 1:
                ++m[0];
                ++m[1];
                break;
            case 2:
                ++m[0];
                break;
            case 3:
                ++m[0];
                --m[1];
                break;
            case 4:
                --m[1];
                break;
            case 5:
                --m[0];
                --m[1];
                break;
            case 6:
                --m[0];
                break;
            case 7:
                --m[0];
                ++m[1];
                break;
        }
        if (m[0] < 0) {
            m[0] = 0;
        }
        else if (m[0] >= mapRow) {
            m[0] = mapRow-1;
        }
        if (m[1] < 0) {
            m[1] = 0;
        }
        else if (m[1] >= mapCol) {
            m[1] = mapCol-1;
        }

        return m;
    }

    /****
     *
     * calculateEnergy -    calculates the amount of energy expended so far. Assume more energy is
     *                      spent when out foraging than when in the nest area.
     *
     * @param currEnergy current energy level
     * @param agentRow agent row position
     * @param agentCol agent column position
     * @return energy level
     */
    public double calculateEnergy (double currEnergy, int agentRow, int agentCol) {

        double energy;

        if (distBetween(agentRow, agentCol, NEST_ROW, NEST_COL) < NEST_SAFETY) {
            energy = currEnergy - ENERGY_EXPENDITURE_RATE/2;
        }
        else {
            energy = currEnergy - currEnergy*ENERGY_EXPENDITURE_RATE;
        }

        return energy;
    }

    /****
     *
     * calculateReward -    calculates the reward based on current energy level.
     *
     * @param currEnergy current energy level
     * @return reward
     */
    public double calculateReward (double currEnergy) {
        return FOOD - currEnergy;
    }

    /****
     *
     * distBetween - returns an integer distance between two grid points. 
     *
     * @param row1 first row position
     * @param col1 first column position
     * @param row2 second row position
     * @param col2 second column position
     * @return distance between two grid points
     */
    public int distBetween (int row1, int col1, int row2, int col2) {
        return (int)Math.sqrt(Math.pow((double)(row1-row2),2.0) + Math.pow((double)(col1-col2),2.0));
    }

    /****
     *
     * getAgent - getter for agent position. 
     *
     * @return current agent position (row, col)
     */
    public int[] getAgent () {
        return this.agent;
    }

    /****
     *
     * getFood - getter for food position. 
     *
     * @return current food position (row, col)
     */
    public int[] getFood () {
        return this.food;
    }

    /****
     *
     * getPred - getter for predator position. 
     *
     * @return current predator position (row, col)
     */
    public int[] getPredator () {
        return this.pred;
    }

    /****
     *
     * getEnergyLevel - getter for energy level. 
     *
     * @return current predator position (row, col)
     */
    public double getEnergyLevel () {
        return this.energyLevel;
    }
    /****
     *
     * setAgent - setter for agent position. 
     *
     * @param row agent row position
     * @param col agent column position
     */
    public void setAgent (int row, int col) {
        this.agent[0] = row;
        this.agent[1] = col;
    }


    /****
     *
     * setFood - setter for food position. 
     *
     * @param row agent row position
     * @param col agent column position
     */
    public void setFood (int row, int col) {
        this.food[0] = row;
        this.food[1] = col;
    }

    /****
     *
     * setPredator - setter for predator position. 
     *
     * @param row agent row position
     * @param col agent column position
     */
    public void setPredator (int row, int col) {
        this.pred[0] = row;
        this.pred[1] = col;
    }

    /****
     *
     * setPredator - setter for predator position. 
     *
     */
    public void setEnergyLevel (double energy) {
        this.energyLevel = energy;
    }

    // to log the current robot position along with the time stamp and append updated info to a file
    public void positionLogNSave(String filename, int distNest, int distPred, int distFood) {
        long currentTimeMS = System.currentTimeMillis();
        String time = Long.toString(currentTimeMS);
        String naming = "time,distNest,distPred,distFood\n";
        String info = time+","+distNest+","+distPred+","+distFood+"\n";
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "FoodPredator");
            if (!root.exists()) {
                root.mkdirs();
            }
            try {
                File file = new File(root, filename);
                if (!file.exists()) {
                    file.createNewFile();
                    fos1=new FileOutputStream(file,true); // make sure the mode allows appending material to the file
                    byte[] n = naming.getBytes();
                    fos1.write(n);
                }
                try {
                    byte[] b = info.getBytes();
                    fos1.write(b);
                } catch (IOException e) {
                    Log.e("app.main","Couldn't write to SD");
                }
            } catch (Exception ex) {
                Log.e("app.main","Couldn't write to SD");
            }
        } catch (Exception e) {
            Log.e("app.main","Couldn't write to SD");
        }
        Log.i("Saved",info);
    }
}