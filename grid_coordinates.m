function grid_coordinates()
grid_size = [10;10];
bl_coord = [-30;-30];
tr_coord = [240;240];
grid_coord = [2;3];
[grid_xs,grid_ys,real_xs,real_ys] = grid_n_real_coords(grid_size,bl_coord,tr_coord);
real_coord = grid2real(grid_xs,grid_ys,real_xs,real_ys,grid_coord);
new_grid_coord = real2grid(grid_xs,grid_ys,real_xs,real_ys,real_coord);
if isequal(grid_coord,new_grid_coord)
    disp('Functions are correct.');
end
new_real_coord = [33.5;-13.5];
[approx_grid_coord,approx_real_coord] = approx_real2grid(grid_xs,grid_ys,real_xs,real_ys,new_real_coord);
end

function [approx_grid_coord,approx_real_coord] = approx_real2grid(grid_xs,grid_ys,real_xs,real_ys,real_coord)
approx_grid_coord = zeros(2,1);
approx_real_coord = zeros(2,1);
[~,indx] = min(abs(bsxfun(@minus,real_coord(1),real_xs)));
approx_grid_coord(1) = grid_xs(indx);
approx_real_coord(1) = real_xs(indx);
[~,indy] = min(abs(bsxfun(@minus,real_coord(2),real_ys)));
approx_grid_coord(2) = grid_ys(indy);
approx_real_coord(2) = real_ys(indy);
end

function real_coord = grid2real(grid_xs,grid_ys,real_xs,real_ys,grid_coord)
real_coord = zeros(2,1);
if ismember(grid_coord(1),grid_xs) && ismember(grid_coord(2),grid_ys)
    real_coord(1) = real_xs(grid_xs==grid_coord(1));
    real_coord(2) = real_ys(grid_ys==grid_coord(2));
else
    error('Please enter a valid 2*1 set of grid coordinates.');
end
end

function grid_coord = real2grid(grid_xs,grid_ys,real_xs,real_ys,real_coord)
grid_coord = zeros(2,1);
if ismember(real_coord(1),real_xs) && ismember(real_coord(2),real_ys)
    grid_coord(1) = grid_xs(real_xs==real_coord(1));
    grid_coord(2) = grid_ys(real_ys==real_coord(2));
else
    error('Please enter a valid 2*1 set of real coordinates.');
end
end

function [grid_xs,grid_ys,real_xs,real_ys] = grid_n_real_coords(grid_size,bl_coord,tr_coord)
x_space = (tr_coord(1)-bl_coord(1))/(grid_size(1)-1);
y_space = (tr_coord(2)-bl_coord(2))/(grid_size(2)-1);
grid_xs = 1:grid_size(1);
grid_ys = 1:grid_size(2);
real_xs = bl_coord(1):x_space:tr_coord(1);
real_ys = bl_coord(2):y_space:tr_coord(2);
end