function [R_x,R_y,R_x_plus,R_y_plus,R_x_minus,R_y_minus,R_x_selected,R_y_selected] = triangulate()
R_x = 10;
R_y = 50;
G_x = -60;
G_y = -60;
B_x = 100;
B_y = -60;

D_g = sqrt((R_x-G_x)^2+(R_y-G_y)^2);
D_b = sqrt((R_x-B_x)^2+(R_y-B_y)^2);

C = calculateC(G_x,G_y,B_x,B_y,D_g,D_b);
k = calculateK(G_x,G_y,B_x,B_y);

m = k^2+1;
n = 2*(k*G_x-C*k-G_y);
p = C^2-2*G_x*C+G_x^2+G_y^2-D_g^2;

delta = calculateDelta(m,n,p);

R_y_plus = (-n+sqrt(delta))/(2*m);
R_y_minus = (-n-sqrt(delta))/(2*m);

R_x_plus = C-k*R_y_plus;
R_x_minus = C-k*R_y_minus;

x_min = -60; x_max = 240; y_min = -60; y_max = 240;
if (R_x_plus >= x_min && R_x_plus <= x_max && R_y_plus >= y_min && R_y_plus <= y_max)
    R_x_selected = R_x_plus;
    R_y_selected = R_y_plus;
elseif (R_x_minus >= x_min && R_x_minus <= x_max && R_y_minus >= y_min && R_y_minus <= y_max)
    R_x_selected = R_x_minus;
    R_y_selected = R_y_minus;
else
    R_x_selected = []; 
    R_y_selected = [];
end
end

function C = calculateC(G_x,G_y,B_x,B_y,D_g,D_b)
C = (G_x^2+G_y^2-B_x^2-B_y^2+D_b^2-D_g^2)/(2*(G_x-B_x));
end

function k = calculateK(G_x,G_y,B_x,B_y)
k = (B_y-G_y)/(B_x-G_x);
end

function delta = calculateDelta(m,n,p)
delta = n^2-4*m*p;
end